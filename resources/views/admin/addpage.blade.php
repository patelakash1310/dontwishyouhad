@extends('layouts.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">

                    <!-- About Me Box -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="col-md-10">
                                <h3 class="box-title"> Add New Page</h3>
                            </div>
                            <div class="col-md-2 ">
                                <a href="javascript:void(0)" onclick="window.history.back();" class="btn btn-block btn-primary btn-sm">Back</a>

                            </div>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <!-- form start -->
                        <form action="{{route('admin.storePage')}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="box-body">

                                <div class="row">
                                    <div class="col-xs-6 {{ $errors->has('page_name') ? 'has-error':'' }}">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Page Name</label>
                                            <input type="text" class="form-control" name="page_name" placeholder="Enter Page Name" value="{{old('page_name')}}">
                                            @if($errors->has('page_name'))
                                                <span class="help-block">{{$errors->first('page_name')}}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-xs-6 {{ $errors->has('title') ? 'has-error':'' }}">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Title</label>
                                            <input type="text" class="form-control" name="title" placeholder="Enter Page Title" value="{{old('title')}}">
                                            @if($errors->has('title'))
                                            <span class="help-block">{{$errors->first('title')}}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6 {{ $errors->has('slug') ? 'has-error':'' }}">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Slug</label>
                                            <input type="text" class="form-control" name="slug" placeholder="Enter Slug" value="{{old('slug')}}">
                                            @if($errors->has('slug'))
                                                <span class="help-block">{{$errors->first('slug')}}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-xs-6 {{ $errors->has('meta_title') ? 'has-error':'' }}">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Meta Title</label>
                                            <input type="text" class="form-control" name="meta_title" placeholder="Enter Meta Title" value="{{old('meta_title')}}">
                                            @if($errors->has('meta_title'))
                                                <span class="help-block">{{$errors->first('meta_title')}}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6 {{ $errors->has('meta_keyword') ? 'has-error':'' }}">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Meta keyword</label>
                                            <textarea type="text" class="form-control" name="meta_keyword" placeholder="Enter Meta Keyword">{{old('meta_keyword')}}</textarea>
                                            @if($errors->has('meta_keyword'))
                                            <span class="help-block">{{$errors->first('meta_keyword')}}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-xs-6 {{ $errors->has('meta_description') ? 'has-error':'' }}">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Meta Description</label>
                                            <textarea type="text" class="form-control" name="meta_description" placeholder="Enter Meta Keyword"> {{old('meta_description')}}</textarea>
                                            @if($errors->has('meta_description'))
                                            <span class="help-block">{{$errors->first('meta_description')}}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4 {{ $errors->has('banner_image') ? 'has-error':'' }}">
                                    <div class="form-group">
                                        <label for="banner_image">Banner Image</label>
                                        <input type="file" class="form-control" name="banner_image" placeholder="" value="{{old('banner_image')}}">
                                        @if($errors->has('banner_image'))
                                            <span class="help-block">{{$errors->first('banner_image')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 ">
                                        <label for="exampleInputEmail1">Page Content</label>
                                            <textarea id="page_content" name="page_content" rows="10" cols="80">
                            {{old('page_content')}}                    </textarea>
                                    </div>
                                </div>

                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box -->

                </div>
                <!--/.col (left) -->

            </div>

        </section><!-- /.content -->

    </div>
    <!-- /.content-wrapper -->
@endsection

@section('js')
<script>
    $(function () {
        $('#example1').DataTable({

        })

        CKEDITOR.replace('page_content');
        CKEDITOR.editorConfig = function( config ) {
            config.extraPlugins = 'preview';
        };
    })
</script>
@endsection
