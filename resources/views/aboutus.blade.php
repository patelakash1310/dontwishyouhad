@extends('layouts.frontmain')

@section('content')

    <section class="banner-wrap">
        <img src="{{asset('frontend/images/abt_inner.jpg')}}" alt="">
        <div class="heading">
            <h1>About Us</h1>
        </div>
        <div class="slantdiv1 hidden-xs"></div>
    </section>

    <section class="about_wrap">
        {!! $aboutus->content !!}
        <div class="slantdiv1 hidden-xs"></div>
    </section>

    <section class="whyus_wrap">
        {!! $whyus->content !!}
        <div class="slantdiv4 hidden-xs"></div>
    </section>

    <section class="quote_wrap">
        {!! $aboutustext->content !!}
    </section>

@endsection