<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class AboutusController extends Controller
{
    // main page
    public function index(){

        $aboutus = Page::where('slug','about-us')->first();
        $whyus = Page::where('slug','why-us')->first();
        $aboutustext = Page::where('slug','about-us-text')->first();

        return view('aboutus',['aboutus'=>$aboutus ,'whyus'=>$whyus ,'aboutustext'=>$aboutustext]);
    }
}
