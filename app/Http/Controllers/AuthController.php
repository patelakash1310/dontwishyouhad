<?php

namespace App\Http\Controllers;


use App\Jobs\SendActivationEmail;
use App\Jobs\SendForgotpasswordEmail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Validator;

class AuthController extends Controller
{
    public function signUp(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'email'=>'required|unique:users',
            'password'=>'required|confirmed',
            'password_confirmation'=>'required|required_with:password',
        ]);

        if ($validator->passes()) {

            User::create([
                'email'=>$request->email,
                'password'=> bcrypt($request->password),
            ]);
            $this->dispatch(new SendActivationEmail($request->email));
            return response()->json(['status' => 1]);
        }

        return response()->json(['message' => $validator->errors(),'status' => 0]);

    }

    public function activateAccount($email)
    {

            if(User::where('email',decrypt($email))->where('status',User::Deactive)->exists())
            {
                $user=User::where('email',decrypt($email))->update(['status' => 1]);
                return redirect(url('/'))->with('success_message','Activated successfully.');
            }else
            {
                return redirect(url('/'))->with('error_message','Alredy activated');
            }
    }



    public function signIn(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'=>'required',
            'password'=>'required',
        ]);

        if ($validator->passes()) {

            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                // Authentication passed...
                //return redirect(route('admin.dashboard'));
                if(Auth::user()->status == 1)
                {
                    return response()->json(['status' => 1]);
                }else
                {
                    return response()->json(['message' => 'Please activate your account.','status' => 2]);
                }

            }else
            {
                return response()->json(['message' => 'Email and Password Invalid.Please try again.','status' => 0]);
            }

        }

        return response()->json(['message' => $validator->errors(),'status' => 2]);
    }

    public function forgotPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'=>'required',
        ]);

        if ($validator->passes()) {

            if (User::where('email',$request->email)->exists()) {

                $this->dispatch(new SendForgotpasswordEmail($request->email));
                return response()->json(['message' => 'Please go to your inbox and use the link we\'ll send you to replace your password.','status' => 1]);

            }else
            {
                return response()->json(['message' => 'Emai address not exist.please enter valid email address.','status' => 0]);
            }
        }

    }

    public function resetPassword($email)
    {
        //dd(decrypt($email));
        return view('resetpassword');
    }


}
