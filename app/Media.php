<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    //
    protected $fillable=['title','description','link','upload_type','image_video','media_date','status'];
}
